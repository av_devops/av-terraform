# Дипломный практикум в Yandex.Cloud

<details><summary>Цели</summary>

1. Подготовить облачную инфраструктуру на базе облачного провайдера Яндекс.Облако.
2. Запустить и сконфигурировать Kubernetes кластер.
3. Установить и настроить систему мониторинга.
4. Настроить и автоматизировать сборку тестового приложения с использованием Docker-контейнеров.
5. Настроить CI для автоматической сборки и тестирования.
6. Настроить CD для автоматического развёртывания приложения.
</details>

  * [Этапы выполнения:](#этапы-выполнения)
     * [Создание облачной инфраструктуры](#создание-облачной-инфраструктуры)
     * [Создание Kubernetes кластера](#создание-kubernetes-кластера)
     * [Создание тестового приложения](#создание-тестового-приложения)
     * [Подготовка cистемы мониторинга и деплой приложения](#подготовка-cистемы-мониторинга-и-деплой-приложения)
     * [Установка и настройка CI/CD](#установка-и-настройка-cicd)

<details><summary>Что необходимо для сдачи задания?</summary> 

1. Репозиторий с конфигурационными файлами Terraform и готовность продемонстрировать создание всех ресурсов с нуля.
2. Пример pull request с комментариями созданными atlantis'ом или снимки экрана из Terraform Cloud или вашего CI-CD-terraform pipeline.
3. Репозиторий с конфигурацией ansible, если был выбран способ создания Kubernetes кластера при помощи ansible.
4. Репозиторий с Dockerfile тестового приложения и ссылка на собранный docker image.
5. Репозиторий с конфигурацией Kubernetes кластера.
6. Ссылка на тестовое приложение и веб интерфейс Grafana с данными доступа.
7. Все репозитории рекомендуется хранить на одном ресурсе (github, gitlab)

</details>

**Перед началом работы над дипломным заданием изучите [Инструкция по экономии облачных ресурсов](https://github.com/netology-code/devops-materials/blob/master/cloudwork.MD).**

---
## Этапы выполнения:

### Создание облачной инфраструктуры
<details><summary>Описание</summary>
Для начала необходимо подготовить облачную инфраструктуру в ЯО при помощи [Terraform](https://www.terraform.io/).

Особенности выполнения:

- Бюджет купона ограничен, что следует иметь в виду при проектировании инфраструктуры и использовании ресурсов;
Для облачного k8s используйте региональный мастер(неотказоустойчивый). Для self-hosted k8s минимизируйте ресурсы ВМ и долю ЦПУ. В обоих вариантах используйте прерываемые ВМ для worker nodes.
- Следует использовать версию [Terraform](https://www.terraform.io/) не старше 1.5.x .

Предварительная подготовка к установке и запуску Kubernetes кластера.

1. Создайте сервисный аккаунт, который будет в дальнейшем использоваться Terraform для работы с инфраструктурой с необходимыми и достаточными правами. Не стоит использовать права суперпользователя
2. Подготовьте [backend](https://www.terraform.io/docs/language/settings/backends/index.html) для Terraform:  
   а. Рекомендуемый вариант: S3 bucket в созданном ЯО аккаунте(создание бакета через TF)
   б. Альтернативный вариант:  [Terraform Cloud](https://app.terraform.io/)  
3. Создайте VPC с подсетями в разных зонах доступности.
4. Убедитесь, что теперь вы можете выполнить команды `terraform destroy` и `terraform apply` без дополнительных ручных действий.
5. В случае использования [Terraform Cloud](https://app.terraform.io/) в качестве [backend](https://www.terraform.io/docs/language/settings/backends/index.html) убедитесь, что применение изменений успешно проходит, используя web-интерфейс Terraform cloud.

Ожидаемые результаты:

1. Terraform сконфигурирован и создание инфраструктуры посредством Terraform возможно без дополнительных ручных действий.
2. Полученная конфигурация инфраструктуры является предварительной, поэтому в ходе дальнейшего выполнения задания возможны изменения.

</details>

### Создание Kubernetes кластера

<details><summary>Описание</summary>
На этом этапе необходимо создать [Kubernetes](https://kubernetes.io/ru/docs/concepts/overview/what-is-kubernetes/) кластер на базе предварительно созданной инфраструктуры.   Требуется обеспечить доступ к ресурсам из Интернета.

Это можно сделать двумя способами:

1. Рекомендуемый вариант: самостоятельная установка Kubernetes кластера.  
   а. При помощи Terraform подготовить как минимум 3 виртуальных машины Compute Cloud для создания Kubernetes-кластера. Тип виртуальной машины следует выбрать самостоятельно с учётом требовании к производительности и стоимости. Если в дальнейшем поймете, что необходимо сменить тип инстанса, используйте Terraform для внесения изменений.  
   б. Подготовить [ansible](https://www.ansible.com/) конфигурации, можно воспользоваться, например [Kubespray](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/)  
   в. Задеплоить Kubernetes на подготовленные ранее инстансы, в случае нехватки каких-либо ресурсов вы всегда можете создать их при помощи Terraform.
2. Альтернативный вариант: воспользуйтесь сервисом [Yandex Managed Service for Kubernetes](https://cloud.yandex.ru/services/managed-kubernetes)  
  а. С помощью terraform resource для [kubernetes](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_cluster) создать **региональный** мастер kubernetes с размещением нод в разных 3 подсетях      
  б. С помощью terraform resource для [kubernetes node group](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_node_group)
  
Ожидаемый результат:

1. Работоспособный Kubernetes кластер.
2. В файле `~/.kube/config` находятся данные для доступа к кластеру.
3. Команда `kubectl get pods --all-namespaces` отрабатывает без ошибок.

</details>

### Создание тестового приложения

<details><summary>Описание</summary>

Для перехода к следующему этапу необходимо подготовить тестовое приложение, эмулирующее основное приложение разрабатываемое вашей компанией.

Способ подготовки:

1. Рекомендуемый вариант:  
   а. Создайте отдельный git репозиторий с простым nginx конфигом, который будет отдавать статические данные.  
   б. Подготовьте Dockerfile для создания образа приложения.  
2. Альтернативный вариант:  
   а. Используйте любой другой код, главное, чтобы был самостоятельно создан Dockerfile.

Ожидаемый результат:

1. Git репозиторий с тестовым приложением и Dockerfile.
2. Регистри с собранным docker image. В качестве регистри может быть DockerHub или [Yandex Container Registry](https://cloud.yandex.ru/services/container-registry), созданный также с помощью terraform.

</details>

### Подготовка cистемы мониторинга и деплой приложения

<details><summary>Описание</summary>

Уже должны быть готовы конфигурации для автоматического создания облачной инфраструктуры и поднятия Kubernetes кластера.  
Теперь необходимо подготовить конфигурационные файлы для настройки нашего Kubernetes кластера.

Цель:
1. Задеплоить в кластер [prometheus](https://prometheus.io/), [grafana](https://grafana.com/), [alertmanager](https://github.com/prometheus/alertmanager), [экспортер](https://github.com/prometheus/node_exporter) основных метрик Kubernetes.
2. Задеплоить тестовое приложение, например, [nginx](https://www.nginx.com/) сервер отдающий статическую страницу.

Способ выполнения:
1. Воспользовать пакетом [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus), который уже включает в себя [Kubernetes оператор](https://operatorhub.io/) для [grafana](https://grafana.com/), [prometheus](https://prometheus.io/), [alertmanager](https://github.com/prometheus/alertmanager) и [node_exporter](https://github.com/prometheus/node_exporter). При желании можете собрать все эти приложения отдельно.
2. Для организации конфигурации использовать [qbec](https://qbec.io/), основанный на [jsonnet](https://jsonnet.org/). Обратите внимание на имеющиеся функции для интеграции helm конфигов и [helm charts](https://helm.sh/)
3. Если на первом этапе вы не воспользовались [Terraform Cloud](https://app.terraform.io/), то задеплойте и настройте в кластере [atlantis](https://www.runatlantis.io/) для отслеживания изменений инфраструктуры. Альтернативный вариант 3 задания: вместо Terraform Cloud или atlantis настройте на автоматический запуск и применение конфигурации terraform из вашего git-репозитория в выбранной вами CI-CD системе при любом комите в main ветку. Предоставьте скриншоты работы пайплайна из CI/CD системы.

Ожидаемый результат:
1. Git репозиторий с конфигурационными файлами для настройки Kubernetes.
2. Http доступ к web интерфейсу grafana.
3. Дашборды в grafana отображающие состояние Kubernetes кластера.
4. Http доступ к тестовому приложению.

</details>

### Установка и настройка CI/CD

<details><summary>Описание</summary>

Осталось настроить ci/cd систему для автоматической сборки docker image и деплоя приложения при изменении кода.

Цель:

1. Автоматическая сборка docker образа при коммите в репозиторий с тестовым приложением.
2. Автоматический деплой нового docker образа.

Можно использовать [teamcity](https://www.jetbrains.com/ru-ru/teamcity/), [jenkins](https://www.jenkins.io/), [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) или GitHub Actions.

Ожидаемый результат:

1. Интерфейс ci/cd сервиса доступен по http.
2. При любом коммите в репозиторие с тестовым приложением происходит сборка и отправка в регистр Docker образа.
3. При создании тега (например, v1.0.0) происходит сборка и отправка с соответствующим label в регистри, а также деплой соответствующего Docker образа в кластер Kubernetes.

</details>

---

### Выполнение

Выбрал сразу систему для построения CI/CD и дальнейшей работы - Gitlab

#### Инфраструктура и мониторинг

  * Создаю проект верхнего уровня [AV_DevOps](https://gitlab.com/av_devops) для размещения будущих двух отдельных проектов под инфраструктуру и тестовое приложение
  * Заполняем раздел Variables раздела CI/CD для использования авторизационных данных для подключения к backend и Yandex Cloud
  * Создаю проект для развертывания инфраструктуры [AV-terraform](https://gitlab.com/av_devops/av-terraform) 
  * Backend в конфигурации Terraform настраиваю на Gitlab.com 
    * [backend](provider.tf) 
    * для инициализации backend на GitLab запустил с локальной машины создание ресурсов сети [сеть](network.tf) и уничтожил ресурсы после  
    
    ![IMG](./scr/tf_state.jpg)


  * Подготавливаю манифесты для разворачивания инфраструктуры и кластера Kubernetes - для разворачивания Kubernetes использую [Yandex Managed Service for Kubernetes](https://cloud.yandex.ru/services/managed-kubernetes) - основные манифесты:
    * [сеть](network.tf)
    * [сервисный аккаунт для k8s](sak8s.tf)
    * [k8s кластер](k8s.tf) и [node группа](k8snodegroup.tf)
  * Для мониторинга кластера буду использовать [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus)
    * копирую манифесты проекта kube-prometheus нужной версии в соответствии с версией кластера Kubernetes [в репозиторий](monitoring/manifests)  
    * для подключения извне к UI Grafana готовлю дополнительный [манифест сервиса](monitoring/grafana-ext-svc.yaml)
  * Далее для работы pipelines по созданию инфраструктуры создаю [файл конфигурации gitlab CI](.gitlab-ci.yml)
  * Для возможности развертывания ресурсов внутри кластера Kubernetes приложений требуется настроить интеграцию с Gitlab через Gitlab agent:
    * создаем [конфигурационный файл](.gitlab/agents/yc-k8s/config.yaml) для GitLab агента
      * нужно отметить, что так как тестовое приложение будет в соседнем проекте, нужно сразу прописать настройку для доступности агента в этом соседнем проекте через общую группу в Gitlab
    * регистрируем в интерфейсе Gitlab агент

    ![IMG](./scr/agent.jpg)

    * запускаем развертывание инфраструктуры через pipeline, делая commit в проект и далее запуская stage apply (ручной запуск во избежание ошибок)

    ![IMG](./scr/tf_apply.jpg)
    ![IMG](./scr/YCresources.jpg)

    * через локальную машину с установленным YC CLI получаем настройки для управления Kubernetes кластером

    ![IMG](./scr/install_agent.jpg)
    ![IMG](./scr/connected_agent.jpg)

  * Развертываем мониторинг через stage monitoring созданного pipeline

  ![IMG](./scr/deploy_monitoring.jpg)

  * Отработавший pipeline по созданию инфраструктуры

  ![IMG](./scr/tf_pipeline.jpg)

  * Проверяем доступность Grafana UI через браузер по IP одной из node и указанному порту 31000 (для экономии использовался NodePort) - [http://158.160.55.181:31000/](http://158.160.55.181:31000/)

  ![IMG](./scr/GrafanaLogin.jpg)
  ![IMG](./scr/grafana_k8sdashboard.jpg)

#### Тестовое приложение

  * Создали второй проект [AV-testapp](https://gitlab.com/av_devops/av-testapp) для тестового приложения в GitLab
  * Подготавливаем файлы для создания image тестового приложения (сервер nginx со статичной html страницей доступной по http)
    * [статичная html страница](https://gitlab.com/av_devops/av-testapp/-/blob/main/files/index.html?ref_type=heads)
    * [картинка для html страницы](https://gitlab.com/av_devops/av-testapp/-/blob/main/files/devops.png?ref_type=heads)
    * [Dockerfile](https://gitlab.com/av_devops/av-testapp/-/blob/main/files/Dockerfile?ref_type=heads)
  * Создаем манифест deploy приложения в кластер Kubernetes - в качестве registry выбран Gitlab Registry
    * [app.yaml](https://gitlab.com/av_devops/av-testapp/-/blob/main/manifest/app.yaml?ref_type=heads)
  * Делаем [файл конфигурации gitlab CI](https://gitlab.com/av_devops/av-testapp/-/blob/main/.gitlab-ci.yml?ref_type=heads) для запуска pipeline для сборки образа и развертывания приложения
    * отмечу, что для возможного запуска из одного места добавлен stage для запуска pipeline проекта [AV-terraform](https://gitlab.com/av_devops/av-terraform) по развертыванию инфраструктуры
  * Делаем commit c tag'ом формата **v0.0.0** и смотрим отработку pipeline по сборке образа и разворачивания приложения

  ![IMG](./scr/build_image.jpg)
  ![IMG](./scr/registry.jpg)
  ![IMG](./scr/deploy_app.jpg)
  ![IMG](./scr/pipeline_app.jpg)

  * Проверяем доступность тестового приложения по публичному адресу [http://158.160.157.68/](http://158.160.157.68/)

  ![IMG](./scr/app.jpg)
  
---
Summary
- Доступ к тестовому приложению по http: [http://158.160.157.68/](http://158.160.157.68/)
- Доступ к UI Grafana по http: [http://158.160.55.181:31000/](http://158.160.55.181:31000/)
  - авторизационные данные приложены при отправке работы
- Проект с тестовым приложением: [AV-testapp](https://gitlab.com/av_devops/av-testapp)
- Проект с разворачиванием инфраструктуры: [AV-terraform](https://gitlab.com/av_devops/av-terraform)
