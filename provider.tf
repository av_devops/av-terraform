terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  backend "http" {
    address         = "https://gitlab.com/api/v4/projects/55480059/terraform/state/terrstate"
    lock_address    = "https://gitlab.com/api/v4/projects/55480059/terraform/state/terrstate/lock"
    unlock_address  = "https://gitlab.com/api/v4/projects/55480059/terraform/state/terrstate/lock"
    username        = "ercuru"
    lock_method     = "POST"
    unlock_method   = "DELETE"
    retry_wait_min  = 5

  }

  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}