resource "yandex_kms_symmetric_key" "kmskey" {
  name              = "kmskey"
  default_algorithm = "AES_128"
  rotation_period   = "8760h"
}
