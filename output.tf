output "k8s_cluster_id" {
  value       = yandex_kubernetes_cluster.k8s.id
  description = "ID of created cluster"
}

output "k8s_external_v4_endpoint" {
  value       = yandex_kubernetes_cluster.k8s.master[0].external_v4_endpoint
  description = "Endpoint for connecting to a cluster"
}
