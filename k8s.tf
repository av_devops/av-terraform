resource "yandex_kubernetes_cluster" "k8s" {
  name       = "k8s"
  network_id = yandex_vpc_network.net.id
  master {
    version   = "1.26"
    public_ip = true

    regional {
      region = "ru-central1"
      location {
        zone      = yandex_vpc_subnet.subnet1.zone
        subnet_id = yandex_vpc_subnet.subnet1.id
      }

      location {
        zone      = yandex_vpc_subnet.subnet2.zone
        subnet_id = yandex_vpc_subnet.subnet2.id
      }

      location {
        zone      = yandex_vpc_subnet.subnet3.zone
        subnet_id = yandex_vpc_subnet.subnet3.id
      }
    }

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        day        = "Wednesday"
        start_time = "23:59"
        duration   = "3h"
      }
    }
  }

  release_channel = "STABLE"

  service_account_id      = yandex_iam_service_account.sak8s.id
  node_service_account_id = yandex_iam_service_account.sak8s.id

  depends_on = [
    yandex_resourcemanager_folder_iam_member.editor,
    yandex_resourcemanager_folder_iam_member.contimagespuller,
    yandex_resourcemanager_folder_iam_member.k8sclustersagent,
    yandex_resourcemanager_folder_iam_member.vpcpublicadmin,
    yandex_vpc_network.net,
    yandex_vpc_subnet.subnet1,
    yandex_vpc_subnet.subnet2,
    yandex_vpc_subnet.subnet3,
    yandex_kms_symmetric_key.kmskey,
  ]

  kms_provider {
    key_id = yandex_kms_symmetric_key.kmskey.id
  }
}
