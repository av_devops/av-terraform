resource "yandex_iam_service_account" "sak8s" {
  name = "sak8s"
  description = "Service account for K8s cluster"
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = var.yc_folder_id
  role = "editor"
  member = "serviceAccount:${yandex_iam_service_account.sak8s.id}"
  depends_on = [
    yandex_iam_service_account.sak8s,
  ]
}

resource "yandex_resourcemanager_folder_iam_member" "k8sclustersagent" {
  folder_id = var.yc_folder_id
  role = "k8s.clusters.agent"
  member = "serviceAccount:${yandex_iam_service_account.sak8s.id}"
  depends_on = [
    yandex_iam_service_account.sak8s,
  ]
}

resource "yandex_resourcemanager_folder_iam_member" "vpcpublicadmin" {
  folder_id = var.yc_folder_id
  role = "vpc.publicAdmin"
  member = "serviceAccount:${yandex_iam_service_account.sak8s.id}"
  depends_on = [
    yandex_iam_service_account.sak8s,
  ]
}

resource "yandex_resourcemanager_folder_iam_member" "contimagespuller" {
  folder_id = var.yc_folder_id
  role = "container-registry.images.puller"
  member = "serviceAccount:${yandex_iam_service_account.sak8s.id}"
  depends_on = [
    yandex_iam_service_account.sak8s,
  ]
}

resource "yandex_resourcemanager_folder_iam_member" "encrypterDecrypter" {
  folder_id = var.yc_folder_id
  role      = "kms.keys.encrypterDecrypter"
  member    = "serviceAccount:${yandex_iam_service_account.sak8s.id}"
  depends_on = [
    yandex_iam_service_account.sak8s,
  ]
}
