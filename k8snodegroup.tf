resource "yandex_kubernetes_node_group" "k8sworkers" {
  cluster_id = yandex_kubernetes_cluster.k8s.id
  name       = "k8sworkers"
  version    = "1.26"


  instance_template {
    platform_id = "standard-v2"

    metadata = {
      ssh-keys = "ubuntu:${file("id_ed25519.pub")}"
    }

    network_interface {
      nat        = true
      subnet_ids = [
        yandex_vpc_subnet.subnet1.id,
        yandex_vpc_subnet.subnet2.id,
        yandex_vpc_subnet.subnet3.id
      ]
    }

    resources {
      memory        = 2
      cores         = 2
      core_fraction = 20
    }

    boot_disk {
      type = "network-hdd"
      size = 30
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 4
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "Wednesday"
      start_time = "04:00"
      duration   = "3h"
    }
  }

  allocation_policy {
    location {
      zone = yandex_vpc_subnet.subnet1.zone
    }
    location {
      zone = yandex_vpc_subnet.subnet2.zone
    }
    location {
      zone = yandex_vpc_subnet.subnet3.zone
    }
  }

  depends_on = [
    yandex_kubernetes_cluster.k8s,
  ]
}
